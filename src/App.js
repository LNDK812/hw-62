import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import Home from './Components/Home/Home.js';
import Contacts from './Components/Contacts/Contacts';
import AboutUs from './Components/AboutUs/AboutUs'
import './App.css';

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path='/' exact component={Home}/>
            <Route path='/contacts' component={Contacts} />
            <Route path='/about' component={AboutUs} />
          </Switch>
        </BrowserRouter>
    )
  }
}

export default App;
