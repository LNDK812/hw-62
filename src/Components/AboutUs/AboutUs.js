import React, {Component} from 'react';
import Header from "../UI/Header/Header";
import './AboutUs.css'


class AboutUs extends Component {
    render() {
        return(
            <div className='HomePage'>
                <div className='Navbar'>
                    <h2 className='TitleText'>Welcome, to our home page!</h2>
                    <div className='ButtonsBlock'>
                        <Header/>
                    </div>
                    <p className='AboutUs'>About Us : </p>
                    <div className='AboutUsText'>
                        We have a large chain of stores throughout the country, more than 10,000 sales every day. You can also get a discount on the promotional code.
                    </div>
                </div>
            </div>
        )
    }
}

export default AboutUs;