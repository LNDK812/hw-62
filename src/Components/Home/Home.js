import React, {Component} from 'react';
import  './Home.css';
import Card from '../UI/Card/Card'
import Header from "../UI/Header/Header";

class Home extends Component {
    render() {
        return(
            <div className='HomePage'>
                <div className='Navbar'>
                    <h2 className='TitleText'>Welcome, to our home page!</h2>
                    <div className='ButtonsBlock'>
                        <Header/>
                    </div>
                    <div className='CardBlock'>
                        <Card/>
                        <Card/>
                        <Card/>
                        <Card/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;