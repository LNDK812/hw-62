import React, {Component} from 'react';
import Card from '../UI/Card/Card'
import './Contacts.css'
import Header from "../UI/Header/Header";

class Contacts extends Component {
    render() {
        return(
            <div className='HomePage'>
                <div className='Navbar'>
                    <h2 className='TitleText'>Welcome, to our home page!</h2>
                   <div className='ButtonsBlock'>
                       <Header/>
                   </div>
                    <h3 className='ContactText'>Our contacts : </h3>
                    <p className='NumberText'>+996 (559) 34 56 78</p>
                    <p className='NumberText'>+996 (772) 21 53 10</p>
                    <p className='NumberText'>+996 (773) 12 23 99</p>
                    <p className='NumberText'>+996 (550) 51 06 63</p>
                </div>
            </div>
        )
    }
}

export default Contacts;