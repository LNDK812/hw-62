import React from 'react';
import { Card, CardImg, CardText, CardBody,
    CardTitle, CardSubtitle, Button } from 'reactstrap';

const Example = (props) => {
    return (
        <div style={{width: '25%',padding : '5px',position : 'relative' , top : '20px' }} className='Card'>
            <Card>
                <CardImg top width="100%" src="https://placeholdit.imgix.net/~text?txtsize=33&txt=318%C3%97180&w=318&h=180" alt="Card image cap" />
                <CardBody>
                    <CardTitle>Iphone XR</CardTitle>
                    <CardSubtitle>1200 $</CardSubtitle>
                    <Button style={{marginTop : '10px'}}>Buy</Button>
                </CardBody>
            </Card>
        </div>
    );
};

export default Example;