import React, {Fragment} from 'react';
import {NavLink} from 'react-router-dom';

const Header = props => {
    return (
        <Fragment>
            <NavLink to='/contacts' className='Button'>Contacts</NavLink>
            <NavLink to='/' className='Button'>Home</NavLink>
            <NavLink to='/about' className='Button'>About Us</NavLink>
        </Fragment>
    )
};

export default Header;